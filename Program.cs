﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
namespace MrRobot
{
    class Program
    {
        public static Robot robot;
        [STAThread]
        static void Main(String[] args)
        {
            resetConsole("Initializing");
            robot = new Robot(getComPort());
            calculateDarkness();
            robot.start();
        }

        /*
         *  Resets console with a given action
         */
        public static void resetConsole(string action = "")
        {
            Console.Clear();
            Console.WriteLine("/////////////////////////////////////////////////////////////");
            Console.WriteLine("//Program is for our final exam. Developed by Malthe, Group 7");
            Console.WriteLine("// Action: " + action);
            Console.WriteLine("/////////////////////////////////////////////////////////////");
            Console.WriteLine();
        }

        /*
         *  Asks the user for a COM Port
         */
        public static String getComPort()
        {
            String comPorts = "";
            new List<string>(SerialPort.GetPortNames()).ForEach(i => comPorts += i + ", ");
            Console.WriteLine(comPorts == "" ? "There Is No Available Com Ports" : "Available Com Ports: " + comPorts);
            return writeAndRead("COM Port: ", SerialPort.GetPortNames().Length > 0 ? SerialPort.GetPortNames()[0] : "").ToUpper(); ;
        }

        /*
         * Calculates what the darkness is
         */
        public static void calculateDarkness()
        {
            resetConsole("Calculate Darkness");
            Console.WriteLine("[Calculate Whiteneess] Hold the robot over the white area, then press a key");
            Console.ReadKey();
            Console.WriteLine("[Calculate Whiteneess] Reading...");
            robot.whiteMax = robot.brick.ReadLight() + 20;
            Console.WriteLine("[Calculate Darkness] Hold the robot over the dark area, then press a key");
            Console.ReadKey();
            Console.WriteLine("[Calculate Darkness] Reading...");
            robot.darkMin = robot.brick.ReadLight() - 20;
            Console.Write("Done!");
        }

        /*
         * This is a smart method that writes to console and returns and answer back.
         */
        public static string writeAndRead(string write, string defaultString = null, bool required = true)
        {
            Console.Write(write + (defaultString == null ? "" : " [" + defaultString + "]") + ": ");
            string ret = Console.ReadLine();
            if (required && ret == "" && defaultString == null)
            {
                Console.WriteLine("This is required! ");
                ret = writeAndRead(write, defaultString, required);
            }
            return ret == "" ? defaultString : ret;
        }
    }
}
