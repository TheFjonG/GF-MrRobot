﻿using System;
using NXT;
using System.Threading;
using System.Windows.Input;
using System.Windows;

namespace MrRobot
{
    class Robot
    {
        public Basicbrick brick;
        private Boolean turningRight;
        public int darkMin, whiteMax;
        private sbyte speed1, speed2;
        private bool awaitingLineup;
        private bool stopped;

        public Robot(string comPort)
        {
            Console.WriteLine("[Robot]: Connecting to device");
            this.brick = NXTBrick.BasicBrick(comPort);
            Console.WriteLine("[Robot]: Connected!");
            this.brick.LightOn();
            Console.WriteLine("[Robot]: Light On");
            this.coastAll();
            Console.WriteLine("[Robot]: Coasted Engines");
        }

        public void start()
        {
            Console.WriteLine("Press a key to start");
            Console.ReadKey();
            Program.resetConsole("Running");
            Thread t =  new Thread(new ThreadStart(lightChecker));
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            normalizeSpeed();
            forward();
        }

        // Normalizes the speed
        private void normalizeSpeed(bool turningRight = true)
        {
            speed1 = 25;
            speed2 = 20;
            this.turningRight = turningRight;
            forward();
        }

        // Sets the speed for each motor with given sbytes and sets the turningRight value.
        private void setSpeed(sbyte speed1, sbyte speed2, bool turningRight)
        {
            this.speed1 = speed1;
            this.speed2 = speed2;
            this.turningRight = turningRight;
            forward();
        }

        public void forward()
        {
            brick.RunEngineA(turningRight ? speed2 : speed1);
            brick.RunEngineB(turningRight ? speed1 : speed2);
        }

        // Stops all engines
        public void coastAll()
        {
            brick.CoastEngineA();
            brick.CoastEngineB();
        }

        // Turns the robot 180 degrees
        public void turnAround()
        {
            setSpeed(20, -20, false);
            //Checks if the sensor hits a dark spot, if it does, continues on
            while (true) if (brick.ReadLight() > darkMin) break;
            normalizeSpeed(false);
        }

        /*
         * Runs in a thread 
         * Checks if th
         */
        
        public void lightChecker()
        {
            while(true)
            {
                if (Keyboard.IsKeyDown(Key.Space))
                {
                    if (stopped)
                    {
                        forward();
                    }
                    else
                    {
                        coastAll();
                    }
                    stopped = !stopped;
                    Thread.Sleep(250);
                    continue;
                }
                if (stopped) continue;

                int light = brick.ReadLight();
                // If in a white spot, turn around
                if(light < whiteMax) turnAround();
                
                if (light < darkMin)
                {
                    if (!awaitingLineup)
                    {
                        setSpeed(speed1, speed2, !turningRight);
                        awaitingLineup = true;
                    }

                }else if(awaitingLineup)
                    awaitingLineup = false;// Called when light > darkMin & awaitingLineup is true
            }
        }

    }
}
